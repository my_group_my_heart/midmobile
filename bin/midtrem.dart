import 'dart:io';

void main(List<String> arguments) {
  print("input : ");
  String txtMat = stdin.readLineSync()!;
  var infixTofix = tokenizerString(txtMat);
  print(infixTofix);
  var listPostfix = postfix(infixTofix);
  print(listPostfix);
}

List<String> tokenizerString(String txtMat) {
  String x = '';
  List<String> list = [];
  for (var i = 0; i < txtMat.length; i++) {
    if (i == txtMat.length && !txtMat[txtMat.length - 1].contains('') ||
        txtMat[i] == '') {
      if (x.isNotEmpty) {
        list.add(x);
        x = '';
      }
    } else {
      x += txtMat[i];
    }
  }
  return list;
}

List<dynamic> infixToPosefix(List token) {
  List<String> work = [];
  List<String> work2 = [];
  List<String> working = ['+', '-', '*', '/', '^'];
  Map<String, int> procedence = {
    '+': 0,
    '-': 0,
    '/': 1,
    '^': 1,
    '(': 2,
    ')': 3,
  };
  for (var i = 0; i < token.length; i++) {
    if (int.tryParse(token[i]) != null) {
      work2.add(token[i]);
    }
    if (working.contains(token[i])) {
      while (work.isNotEmpty &&
          work.last != '(' &&
          procedence[token[i]]! < procedence[token.last]!) {
        work2.add(work.removeLast());
      }
      work.add(token[i]);
    }
    if (token[i] == '(') {
      work.add(token[i]);
    }
    if (token[i] == ')') {
      while (work.last != '(') {
        work2.add(work.removeLast());
      }
      work.removeLast();
    }
  }
  while (work.isNotEmpty) {
    work2.add(work.removeLast());
  }
  return work2;
}

List postfix(List listPostfix) {
  var values = [];
  for (int i = 0; i < listPostfix.length; i++) {
    if ((listPostfix[i])) {
      values.add(listPostfix[i]);
    } else {
      int n1 = int.parse(values[values.length - 2]);
      int n2 = int.parse(values[values.length - 1]);
      values.removeAt(values.length - 2);
      values.removeAt(values.length - 1);
      if (listPostfix[i] == "+") {
        values.add((n1 + n2).toString());
      } else if (listPostfix[i] == "-") {
        values.add((n1 - n2).toString());
      } else if (listPostfix[i] == "*") {
        values.add((n1 * n2).toString());
      } else if (listPostfix[i] == "/") {
        values.add((n1 / n2).toInt().toString());
      }
    }
  }
  return values;
}
